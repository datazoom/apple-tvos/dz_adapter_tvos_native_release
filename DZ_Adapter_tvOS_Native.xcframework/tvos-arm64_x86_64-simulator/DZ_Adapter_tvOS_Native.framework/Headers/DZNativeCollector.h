//
//  DZNativeCollector.h
//  DZNativeCollector
//
//  Created by Momcilo Stankovic on 06/05/21.
//  Copyright © 2021 RedCellApps. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DZNativeCollector.
FOUNDATION_EXPORT double DZNativeCollectorVersionNumber;

//! Project version string for DZNativeCollector.
FOUNDATION_EXPORT const unsigned char DZNativeCollectorVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DZNativeCollector/PublicHeader.h>


