# DZ_Adapter_tvOS_Native_Release



Instructions for Customers to add tvOS Native Framework in your tvOS Application.

The tvOS Media Player framework allows access to the native media player included with tvOS. Datazoom’s framework facilitates native tvOS applications to send video playback events based on the configuration created in data pipes.
Initial setup

    The DZNativeCollector.framework file should be downloaded from https://gitlab.com/datazoom/apple-tvos/dz_adapter_tvos_native_release

    Drag and drop the downloaded framework file into your Xcode project. Drag the file to the files inspector on the left side of Xcode. Make sure you check the box "Copy items" in the popup menu that displays while drag and drop the file.
    Make sure that the framework added in the above step is present at the Embedded Binaries section under the General. If not present, add the framework by clicking the "+" button available at the same section

Steps for Swift Language based Applications:

    After including the framework file, open the ViewController/View file, where the AVPlayer(native player) is included.

    Import the framework using the following command:

import DZ_Adapter_tvOS_Native

    Initialize the framework by passing along the 'Configuration ID', and player instance:

DZNativeCollector.dzSharedManager.initNativePlayerWith(configID: <configuration id from Datazoom>, url: <url given by Datazoom>, playerInstance: <AVPlayer object>)

    Run the app and observe the events captured in a Connector, data corresponding to OTT/tvOS in Platform, refers to the events tracked from your OTT/tvOS device.
    To send Custom events and/or Custom metadata to Datazoom SDK we can use following methods:

DZNativeCollector.dzSharedManager.customEvents("Some custom event", metadata: "Some custom metadata")

    Change "Some custom event" and "Some custom metadata" to any text or variable you want to send to collector service.

Steps for ObjectiveC Language based Applications:

    After including the framework file, Create a bridging header file, to allow interoperability of languages.

    open the ViewController/View file, where the AVPlayer(native player) is included.

    Import the following:

#import <AVKit/AVKit.h> #import <AVFoundation/AVFoundation.h> #import <DZNativeConnector/DZNativeConnector.h>

    Initialize the swift class in the .h file.

DZNativeConnector *dzObject;

    In the .m file, allocate using:

dzObject = [[DZNativeConnector alloc]init];
[dzObject initNativePlayerWithConfigId: <configuration id from Datazoom>, url: <url given by Datazoom>, playerInstance: <AVPlayer Object>];

    Run the app and observe the events captured in a Connector, data corresponding to OTT/tvOS in Platform, refers to the events tracked from your tvOS device

Demo Application

    A demo application that shows the usage of this framework is available Here.  This can be used to test the DZ_Adapter_tvOS_Native.Framework
